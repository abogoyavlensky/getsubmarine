import os
import requests

from PIL import Image
from StringIO import StringIO

SCALE = 6  # scale could be range 2 - 6

Y_RANGE = (0, 64)
X_RANGE = (8, 56)

TEMP_DIR = 'temp'
RESULT_DIR = 'map'

RESULT_FORMAT = '.png'

BASE_MAP_URL = ('http://{source}.tiles.telegeography.com/maps/',
                'submarine-cable-map-2015/{scale}/{y}/{x}.png')

SIZE = 256  # pieces of picture should be square

if __name__ == '__main__':
    if not os.path.exists(TEMP_DIR):
        os.makedirs(TEMP_DIR)

    if not os.path.exists(RESULT_DIR):
        os.makedirs(RESULT_DIR)

    # fetch picture by pieces
    for y in xrange(*Y_RANGE):
        for x in xrange(*X_RANGE):
            url = BASE_MAP_URL.format(source='a', scale=SCALE, y=y, x=x)
            response = requests.get(url)
            if response.ok:
                name = os.path.join(TEMP_DIR, '{y}_{x}.png'.format(y=y, x=x))
                i = Image.open(StringIO(response.content))
                i.save(name)
                print 'Piece with coords: (y={y},x={x}) was fetched.'\
                    .format(y=y, x=x)
            else:
                print 'Something went wrong with fetching image by url {0}'\
                    .format(url)

    # create result image from saved pieces
    new_im = Image.new('RGB', (SIZE * Y_RANGE[1],
                               SIZE * (X_RANGE[1] - X_RANGE[0])))
    for y in xrange(*Y_RANGE):
        for x in xrange(*X_RANGE):
            relate_x = x - X_RANGE[0]
            im = Image.open(os.path.join(TEMP_DIR, '{y}_{x}.png'
                                         .format(y=y, x=x)))
            new_im.paste(im, (y * SIZE, relate_x * SIZE))

            print 'Piece with coords: (y={y},x={x}) was joined.'\
                .format(y=y, x=x)

    new_im.save(os.path.join(RESULT_DIR, 'map{0}'.format(RESULT_FORMAT)))
    print 'Map has been created. Please check result image in map/ dir.'